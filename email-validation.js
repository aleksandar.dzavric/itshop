/*
<!-- Javascript e-mail validation  -->
function validation(){
    var email = document.getElementById('email').value;
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    var pom = document.getElementById('area').value;
    var demo = document.getElementById('demo');

    if (!reg.test(email))
    {
        demo.innerHTML = "<span style='color:red;'> Pogresna e-mail adresa, pokušajte ponovo! </span>";
    }
    else if(pom == ""){
        demo.innerHTML = "<span style='color:red;'> Morate uneti tekst u tekst areu! </span>";
    }
    else{
        demo.innerHTML = "<span style='color:green;'> Hvala na povratnim informacijama! </span>";
    }
}
*/
    /* ---------- AJAX validation ----------  */
    let btnSendMessage = document.getElementById('sendMessage');
    let emailInput = document.getElementById('email');
    let areaInput = document.getElementById('area');
    let errorMessage = document.getElementById('error-msg');
    let form = document.getElementById('form');
    console.log(form);

    form.addEventListener('submit', (event) => {
        event.preventDefault();
    });    
    
    btnSendMessage.addEventListener('click', (event)=>{
        const request = new XMLHttpRequest();
        request.onload = function(){
            const responseObj = JSON.parse(request.responseText);
            
            if(responseObj.validEmpty){
                errorMessage.innerHTML = "<span style='color:red'>Morate popuniti polje za email!</span>";
            }
            else if(!responseObj.validFormat){
                errorMessage.innerHTML = "<span style='color:red'>Neispravan format mejla!</span>";
            }
            else if(responseObj.validAreaEmpty){
                errorMessage.innerHTML = "<span style='color:red'>Morate uneti komentar pre slanja feedback-a</span>"
            }
            else{
                errorMessage.innerHTML = "<span style='color:green'>Uspesno ste nam poslali feedback!</span>";
            }
        }
        request.open("GET", `check-email.php?email=${emailInput.value}&area=${areaInput.value}`);
        request.send();
    });

