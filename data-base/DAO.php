<?php
require_once 'db.php';

class DAO {
	private $db;

	// za 2. nacin resenja
	// private $INSERTOSOBA = "INSERT INTO osoba (ime, prezime, JMBG, vremeUpisa) VALUES (?, ?, ?, CURRENT_TIMESTAMP)";
	// private $DELETEOSOBA = "DELETE  FROM osoba WHERE idosoba = ?";
	// private $SELECTBYID = "SELECT * FROM osoba WHERE idosoba = ?";	
	// private $GETLASTNOSOBA = "SELECT * FROM osoba ORDER BY idosoba DESC LIMIT ?";

	private $SELECT_ALL_PRODUSCTS = "SELECT * FROM products";
	
	public function __construct()
	{
		$this->db = DB::createInstance();
	}

	public function getProducts()
	{	
		$statement = $this->db->prepare($this->SELECT_ALL_PRODUSCTS);
		$statement->execute();
		
		$result = $statement->fetchAll();
		return $result;
	}

	public function insertProduct($type, $name, $price, $quantity)
	{
		$statement = $this->db->prepare($this->INSERTOSOBA);
		$statement->bindValue(1, $type);
		$statement->bindValue(2, $name);
		$statement->bindValue(3, $price);
		$statement->bindValue(4, $quantity);
		
		$statement->execute();
	}

	public function deleteOsoba($idosoba)
	{
		$statement = $this->db->prepare($this->DELETEOSOBA);
		$statement->bindValue(1, $idosoba);
		
		$statement->execute();
	}

	public function SelectUserByUserNameAndPassword($username, $password)
	{
		$statement = $this->db->prepare($this->SELECT_USER_BY_USERNAME_PASSWORD);
		$statement->bindValue(1, $username);
		$statement->bindValue(2, $password);
		
		$statement->execute();
		
		$result = $statement->fetch();
		return $result;
	}

	public function SelectUserByUserName($username)
	{
		$statement = $this->db->prepare($this->SELECT_USER_BY_USERNAME);
		$statement->bindValue(1, $username);
		
		$statement->execute();
		
		$result = $statement->fetch();
		return $result;
	}
}
?>
