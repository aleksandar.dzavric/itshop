<?php
    $valid = isset($valid)? $valid:"";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
    <link rel="stylesheet" href="style.css">
    <title>Kontakt</title>
</head>
<body>
    <?php
        include_once './partials/topPartial.php';
    ?>
    <!--navigation-->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <div class="dropdown">
                            <button class="btn browse-category  dropdown-toggle" type="button" id="dropdownMenuButton2"
                                data-bs-toggle="dropdown" aria-expanded="false">
                                <small>BROWSE CATEGORIES</small>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton2">
                                <li><a class="dropdown-item" href="#">Action</a></li>
                                <li><a class="dropdown-item" href="#">Another action</a></li>
                                <li><a class="dropdown-item" href="#">Something else here</a></li>
                                <li>
                                    <hr class="dropdown-divider">
                                </li>
                                <li><a class="dropdown-item" href="#">Separated link</a></li>
                            </ul>
                        </div>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-bs-toggle="dropdown" aria-expanded="false">
                                HOME
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="index.php">Go to Home Page</a></li>
                                <li><a class="dropdown-item" href="#">Another action</a></li>
                                <li><hr class="dropdown-divider"></li>
                                <li><a class="dropdown-item" href="#">Something else here</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-bs-toggle="dropdown" aria-expanded="false">
                                SHOP
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="shop.php">Shop page</a></li>
                                <li><a class="dropdown-item" href="#">Another action</a></li>
                                <li><hr class="dropdown-divider"></li>
                                <li><a class="dropdown-item" href="#">Something else here</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-bs-toggle="dropdown" aria-expanded="false">
                                PAGES
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="#">Action</a></li>
                                <li><a class="dropdown-item" href="#">Another action</a></li>
                                <li>
                                    <hr class="dropdown-divider">
                                </li>
                                <li><a class="dropdown-item" href="#">Something else here</a></li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link ">BLOGS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="contact.php">CONTACT</a>
                        </li>
                    </ul>
                    <form class="d-flex">
                        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                        <button class="btn btn-outline-success" type="submit">Search</button>
                    </form>
                </div>
            </div>
        </div>
    </nav>

    <div class="container-fluid contact-main">
        <div class="container">
            <div class="row py-5">
                <div class="col-lg-4">
                    <h5>STORE INFORMATION</h5>
                    <div class="row">
                        <div class="col-lg-1 d-flex align-items-center">
                            <i class="bi bi-geo-alt-fill"></i>
                        </div>
                        <div class="col-lg-11">
                            <small>Leo Technology</small><br>
                            <small>United States</small>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 d-flex align-items-center">
                            <i class="bi bi-envelope-fill"></i>
                        </div>
                        <div class="col-lg-11">
                            <small>Email us:</small><br>
                            <small>demo@demo.com</small>
                        </div>
                    </div>
                    <hr>
                    <br>
                </div>
                <div class="col-lg-8 map-column">
                    <!--Dodavanje google strane-->
                    <p>Our Location</p>
                    <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2831.602487725348!2d20.45727651742223!3d44.78890882545587!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475a70145b2a7897%3A0x228ef2242b2f9dd2!2z0J_QsNGA0YLQuNC30LDQvdC-0LIg0YHRgtCw0LTQuNC-0L0!5e0!3m2!1ssr!2srs!4v1653093298501!5m2!1ssr!2srs" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe> -->

                    <div class="container send-message my-5">
                        <h3>Drop Us A Line</h3>
                        <p class="text-muted">
                            Have a qquestion or comment? Use the form below to send us a message or contact us by email
                        </p>
                        <form action="" method="get" id="form">
                            <select>
                                    <option value="prva">Prvi</option>
                                    <option value="druga">Drugi</option>
                                    <option value="treca">Treci</option>
                                    <option value="cetvrti">Cetvrti</option>
                            </select>
                            <input type="text" id="email" name="email" placeholder="your@email.com"><br><br>
                            <input type="file"> <br> <br>
                            <textarea id="area" name="area" placeholder="How can we help you?"cols="30" rows="4"></textarea>
                            <br><br>
                            <!-- <input type="submit" id="sendMessage" class="send-message-btn"  value="Post Comment" onclick="validation()"> -->
                            <button type="submit" id="sendMessage">Posalji</button>
                        </form>
                        <div class="error-msg" id="error-msg">
                            
                        </div>
                        <div id="demo">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
        include_once './partials/bottomPartial.php';
    ?>
        <script src="email-validation.js"></script>
</body>
</html>