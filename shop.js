let itshop = [
    {
        "type": "mobiles",
        "name": "Samsung Galaxy J5",
        "price": 220
    },
    {
        "type": "laptops",
        "name": "Acer Aspire 5",
        "price": 500
    },
    {
        "type": "laptops",
        "name": "HP Series 5",
        "price": 550
    },
    {
        "type": "headphones",
        "name": "Siberia v3",
        "price": 80
    },
    {
        "type": "mobiles",
        "name": "Sony Xperia M4",
        "price": 280
    },
    {
        "type": "mobiles",
        "name": "Iphone 13",
        "price": 960
    },
    {
        "type": "laptops",
        "name": "Asus PRO",
        "price": 630
    },
    {
        "type": "mobiles",
        "name": "Samsung S22",
        "price": 740
    },
    {
        "type": "mobiles",
        "name": "Wikoo JSPL",
        "price": 150
    },
    {
        "type": "headphones",
        "name": "Schorpion 4353",
        "price": 110
    },
    {
        "type": "laptops",
        "name": "DELL hzsy",
        "price": 220
    },
];

var productList = document.getElementById('product-list');
//console.log(productList);
var tmp = '';

//itshop.forEach(element => {console.log(element);});
//all items on page
itshop.forEach(element => {
    tmp+="<div class='col-lg-4 d-flex flex-column'><h1>"+element.type+"</h1><br><p>"+element.name+"</p><br><p>"+element.price+"</p></div>";
});
//console.log(tmp);
document.getElementById('product-list').innerHTML = tmp;


//checkbox
var checkedArray = ['mobiles','laptops', 'headphones'];
console.log(checkedArray);

let checkboxMobile = document.getElementById("checkbox-mobile");
let checkboxLaptop = document.getElementById("checkbox-laptop");
let checkboxHeadphones = document.getElementById("checkbox-headphones");

checkboxMobile.addEventListener('change', (event)=>{
    let index = checkedArray.indexOf('mobiles');
    if(event.currentTarget.checked){
        checkedArray.push(checkboxMobile.value);
        console.log(checkedArray);
    }
    else{
        checkedArray.splice(index, 1);
        console.log(checkedArray);
    }
    ReturnProductsWithCheckedType();
});
checkboxLaptop.addEventListener('change', (event)=>{
    let index = checkedArray.indexOf('laptops');
    if(event.currentTarget.checked){
        checkedArray.push(checkboxLaptop.value);
        console.log(checkedArray);
    }
    else{
        checkedArray.splice(index, 1);
        console.log(checkedArray);
    }
    ReturnProductsWithCheckedType();
});

checkboxHeadphones.addEventListener('change', (event)=>{
    let index = checkedArray.indexOf('headphones');
    if(event.currentTarget.checked){
        checkedArray.push(checkboxHeadphones.value);
        console.log(checkedArray);
    }
    else{
        checkedArray.splice(index, 1);
        console.log(checkedArray);
    }
    ReturnProductsWithCheckedType();
});

function ReturnProductsWithCheckedType(){
    let listProducts = '';
    itshop.forEach(element =>{
        for(let i = 0; i<checkedArray.length; i++){
            if(checkedArray[i]==element.type){
                listProducts+="<div class='col-lg-4 d-flex flex-column'><h1>"+element.type+"</h1><br><p>"+element.name+"</p><br><p>"+element.price+"</p></div>";                ;
            }
        }
    });
    document.getElementById('product-list').innerHTML = listProducts;
}

//search
function SearchProduct(){
    listProducts="";
    var searchInput = document.getElementById('search-input').value;
    //alert(searchInput);

    itshop.forEach(element =>{
      if(element.name.toLowerCase().includes(searchInput)){
        listProducts+="<div class='col-lg-4 d-flex flex-column'><h1>"+element.type+"</h1><br><p>"+element.name+"</p><br><p>"+element.price+"</p></div>";
      }
    });
    document.getElementById('product-list').innerHTML = listProducts;
}

//slider 
let rangeSlider = document.getElementById('range-slider');
let value = document.getElementById("value");
value.textContent = rangeSlider.value + " $";

rangeSlider.oninput = function(){
    let listProducts = '';
    value.textContent = this.value;
    value.textContent += " $";
    itshop.forEach(element => {
        if(element.price<=this.value){
            listProducts+="<div class='col-lg-4 d-flex flex-column'><h1>"+element.type+"</h1><br><p>"+element.name+"</p><br><p>"+element.price+"</p></div>";
        }
    });
    document.getElementById('product-list').innerHTML = listProducts;
}