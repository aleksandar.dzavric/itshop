<!--------------------FOOTHER-------------------->
<footer>
    <div class="container-fluid bg-dark text-light">
        <div class="container">
            <div class="row p-4 my-3 fear align-items-center">
                <div class="col-lg-4 fear-first">
                    <div class="row">
                        <div class="col-lg-2 icon d-flex align-items-center">
                            <i class="fa-regular fa-envelope"></i>
                        </div>
                        <div class="col-lg-10 w-95">
                            <p>
                                <b>Fear Of Missing Out?</b> <br>
                                <span>Get the latest deals, update and more</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <form class="d-flex">
                        <input class="form-control me-2" type="search" placeholder="Your email address"
                            aria-label="Subscribe">
                        <button class="btn subscribe" type="submit">Subscribe</button>
                    </form>
                </div>
                <div class="col-lg-4 follow text-white">
                    <b>FOLLOW US</b> &nbsp;
                    <i class="fa-brands fa-facebook"></i>
                    <i class="fa-brands fa-twitter"></i>
                    <i class="fa-brands fa-youtube"></i>
                    <i class="fa-brands fa-instagram"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-3 company">
                        <h5>COMPANY</h5><br>
                        <p>About Us</p>
                        <p>Careers</p>
                        <p>Affiliates</p>
                        <p>Blog</p>
                        <p>Contact Us</p>
                    </div>
                    <div class="col-lg-3 shop">
                        <h5>SHOP</h5><br>
                        <p>Televisions</p>
                        <p>Fridges</p>
                        <p>Washing Machine</p>
                        <p>Fans</p>
                        <p>Air Conditioners</p>
                        <p>Laptops</p>
                    </div>
                    <div class="col-lg-3 help">
                        <h5>HELP</h5><br>
                        <p>Customer Service</p>
                        <p>My Account</p>
                        <p>Find a Store</p>
                        <p>Legal & Privacy</p>
                        <p>Contact</p>
                        <p>Gift Card</p>
                    </div>
                    <div class="col-lg-3 myacc">
                        <h5>MY ACCOUNT</h5><br>
                        <p>My Profile</p>
                        <p>My Order History</p>
                        <p>My Wish List</p>
                        <p>Order Tracking</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 contact">
                <h5>CONTACT INFO</h5>
                <p><b>(+84)-01234-5678</b>
                    <br>
                    <small class="text-muted">Mon to Sun: 10:00AM To 6:00 PM</small>
                </p>
                <p class="demo">demo@demo.com</p>
                <hr>
                <p class="text-muted">48 West Temple Drive</p>
                <p class="text-muted">Ashburn, VA 20147</p>
            </div>
        </div>
    </div>
    <div class="container">
        <hr>
        <div class="row px-5">
            <div class="col-lg-3  support-main">
                <div class="support-icon col-sm-2">
                    <i class="bi bi-telephone-forward"></i>
                </div>
                <div class="support-text col-sm-10">
                    <b>Fast Delivery</b>
                    <p class="text-muted">For All Orders Over $120</p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-12 support-main">
                <div class="support-icon col-sm-2">
                    <i class="bi bi-arrow-return-left"></i>
                </div>
                <div class="support-text col-sm-10">
                    <b>Free Return</b>
                    <p class="text-muted">For Return Over $100</p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-12 support-main">
                <div class="support-icon col-sm-2">
                    <i class="bi bi-telephone-inbound-fill"></i>
                </div>
                <div class="support-text col-sm-10">
                    <b>Customer Support</b>
                    <p class="text-muted">Friendly Customer</p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-12 support-main">
                <div class="support-icon col-sm-2">
                    <i class="bi bi-wallet2"></i>
                </div>
                <div class="support-text col-sm-10">
                    <b>Money Back Guarante</b>
                    <p class="text-muted">Quality Checked</p>
                </div>
            </div>
        </div>
        <hr>
        <div class="row copyright">
            <div class="col-lg-4">
                <p>Copyright &copy; Technology. All rights reserved</p>
            </div>
            <div class="col-lg-8 d-flex second-column">
                <div class="col-lg-3 ">
                    <p>Private Policy</p>
                </div>
                <div class="col-lg-3">
                    <p>Terms & Condintions</p>
                </div>
                <div class="col-lg-2">
                    <p>Cookie</p>
                </div>
                <div class="col-lg-4">
                    <p>Icons &nbsp; Icon2 &nbsp; Icon 3 &nbsp; Icon 4 &nbsp;</p>
                </div>
            </div>
        </div>
    </div>
</footer>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
    crossorigin="anonymous"></script>