<!--HEADER-->
<header class="navbar navbar-expand-lg navbar-dark bg-dark ">
        <div class="container">
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="#">Shipping</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">FAQ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Contact</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Track Order</a>
                        </li>
                    </ul>
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="#"><i class="bi bi-check2-circle"></i> Fee 30 Days Money Back
                                Guarantee
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button"
                                data-bs-toggle="dropdown" aria-expanded="false">
                                USD
                            </a>
                            <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                                <li><a class="dropdown-item" href="#">RSD</a></li>
                                <li><a class="dropdown-item" href="#">GBR</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button"
                                data-bs-toggle="dropdown" aria-expanded="false">
                                English
                            </a>
                            <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                                <li><a class="dropdown-item" href="#">Serbian</a></li>
                                <li><a class="dropdown-item" href="#">Руски</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
</header>
    <div class="container my-1">
        <div class="row">
            <div class="col-lg-3">
                <a href="index.php"><img src="./img/technogy.png" alt="technogy"></a>
            </div>
            <div class="col-lg-3">
                <p> <b>Send us a message</b> <br>
                    <u>demo@dmo.com</u>
                </p>

            </div>
            <div class="col-lg-3">
                <p>Need help? Call us <br>
                    <b>012 345 6789</b>
                </p>
            </div>
            <div class="col-lg-3 header-icons">
                <i class="bi bi-person"></i>&nbsp; 0&nbsp;&nbsp;&nbsp;
                <i class="bi bi-heart"></i></i>&nbsp; 0&nbsp;&nbsp;&nbsp;
                <i class="bi bi-bag"></i></i>&nbsp; 0&nbsp;&nbsp;&nbsp;
            </div>
        </div>
    </div>
    