<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
    <link rel="stylesheet" href="style.css">
    <title>Home</title>
</head>
<body>
<?php
    include_once './partials/topPartial.php';
?>
<!-------------------- NAVIGATION -------------------->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <div class="dropdown">
                            <button class="btn browse-category  dropdown-toggle" type="button" id="dropdownMenuButton2"
                                data-bs-toggle="dropdown" aria-expanded="false">
                                <small>BROWSE CATEGORIES</small>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton2">
                                <li><a class="dropdown-item" href="#">Action</a></li>
                                <li><a class="dropdown-item" href="#">Another action</a></li>
                                <li><a class="dropdown-item" href="#">Something else here</a></li>
                                <li>
                                    <hr class="dropdown-divider">
                                </li>
                                <li><a class="dropdown-item" href="#">Separated link</a></li>
                            </ul>
                        </div>
                        <li class="nav-item dropdown ">
                            <a class="nav-link active dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-bs-toggle="dropdown" aria-expanded="false">
                                HOME
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="#">Action</a></li>
                                <li><a class="dropdown-item" href="#">Another action</a></li>
                                <li><hr class="dropdown-divider"></li>
                                <li><a class="dropdown-item" href="#">Something else here</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-bs-toggle="dropdown" aria-expanded="false">
                                SHOP
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="shop.php">Shop Page</a></li>
                                <li><a class="dropdown-item" href="#">Another action</a></li>
                                <li><hr class="dropdown-divider"></li>
                                <li><a class="dropdown-item" href="#">Something else here</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-bs-toggle="dropdown" aria-expanded="false">
                                PAGES
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="#">Action</a></li>
                                <li><a class="dropdown-item" href="#">Another action</a></li>
                                <li>
                                    <hr class="dropdown-divider">
                                </li>
                                <li><a class="dropdown-item" href="#">Something else here</a></li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link ">BLOGS</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="contact.php">CONTACT</a>
                        </li>
                    </ul>
                    <form class="d-flex">
                        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                        <button class="btn btn-outline-success" type="submit">Search</button>
                    </form>
                </div>
            </div>
        </div>
    </nav>
    <div class="container-fluid ">
        <div class="container">
            <div class="row d-flex ">
                <div class="col-md-8 d-flex align-items-center justify-content-center">
                    <img src="./img/phone.jpeg" class="img-fluid" alt="moilni" width="100%" height="85%">
                </div>
                <div class="col-md-4 py-5">
                    <div class="row py-2" style="height:50%">
                        <img src="./img/mac.jpg" alt="mac" height="100%">
                    </div>
                    <div class="row py-2" style="height:50%">
                        <img src="./img/headphones.jpeg" alt="headphones" height="100%">
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="container-fluid bg-light">
        <div class="container">
            <div class="row my-3">
                <h5>Our Fetured Offers</h5>
                <div class="col-lg-3">
                    <div class="card h-100">
                        <img src="./img/phone.jpeg" class="card-img-top" alt="...">
                        <div class="card-body">
                            <p>MOBILE</p>
                            <h5 class="card-title">Samsung Galaxz M51</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk
                                of the card's content.</p>
                            <p class="card-text"> <s>$23.90</s> $19.20</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card h-100">
                        <img src="./img/mac.jpg" class="card-img-top" alt="...">
                        <div class="card-body">
                            <p>MOBILE</p>
                            <h5 class="card-title">Samsung Galaxz M51</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk
                                of the card's content.</p>
                            <p class="card-text"> <s>$23.90</s> $19.20</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card h-100">
                        <img src="./img/headphones.jpeg" class="card-img-top" alt="...">
                        <div class="card-body">
                            <p>MOBILE</p>
                            <h5 class="card-title">Samsung Galaxz M51</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk
                                of the card's content.</p>
                            <p class="card-text"> <s>$23.90</s> $19.20</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card h-100">
                        <img src="./img/phone.jpeg" class="card-img-top" alt="...">
                        <div class="card-body">
                            <p>MOBILE</p>
                            <h5 class="card-title">Samsung Galaxz M51</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk
                                of the card's content.</p>
                            <p class="card-text"> <s>$23.90</s> $19.20</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row my-5">
                <div class="bgred">
                    <h5>This Week Deals</h5>
                    <p>307days:15hours:35min:13sek</p>
                </div>
                <div class="col-lg-6">
                    <div class="card mb-3">
                        <div class="row g-0">
                            <div class="col-md-7 p-3 d-flex justify-content-center align-items-center">
                                <img src="./img/mac.jpg" class="img-fluid rounded-start w-60 h-60" alt="headphones">
                            </div>
                            <div class="col-md-5">
                                <div class="card-body py-5">
                                    <p class="card-text"> <span class="text-muted">MAC</span> <br> <b>Apple MAC
                                            24424</b> </p>
                                    <i class="bi bi-star"></i>
                                    <i class="bi bi-star"></i>
                                    <i class="bi bi-star"></i>
                                    <i class="bi bi-star"></i>
                                    <i class="bi bi-star"></i>
                                    <p class="card-text"><small><s class="text-muted">$23.90</s> &nbsp;
                                            <b>$19.2</b></small></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card mb-3">
                        <div class="row g-0">
                            <div class="col-md-7 p-3 d-flex justify-content-center align-items-center">
                                <img src="./img/mac.jpg" class="img-fluid rounded-start w-60 h-60" alt="headphones">
                            </div>
                            <div class="col-md-5">
                                <div class="card-body py-5">
                                    <p class="card-text"> <span class="text-muted">MAC</span> <br> <b>Apple MAC
                                            24424</b> </p>
                                    <i class="bi bi-star"></i>
                                    <i class="bi bi-star"></i>
                                    <i class="bi bi-star"></i>
                                    <i class="bi bi-star"></i>
                                    <i class="bi bi-star"></i>


                                    <p class="card-text"><small><s class="text-muted">$23.90</s> &nbsp;
                                            <b>$19.2</b></small></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-2"><img src="./img/camerax.png" alt="camerax"></div>
            <div class="col-lg-2"><img src="./img/computerup.png" alt="computerup"></div>
            <div class="col-lg-2"><img src="./img/digital-store.png" alt="digital-store"></div>
            <div class="col-lg-2"><img src="./img/digitech.png" alt="digitech"></div>
            <div class="col-lg-2"><img src="./img/eletronics-retailer.png" alt="eletronics-retailer"></div>
            <div class="col-lg-2"><img src="./img/games-spot.png" alt="games-spot"></div>
        </div>
    </div>
    <?php
        include_once './partials/bottomPartial.php';
    ?>
</body>

</html>

<?php
    $products = [
        [
            "type"=> "laptop",
            "name"=>"HP Series 5",
            "price"=> 550,
            "active"=>false,
        ],
        [
            "type"=> "headphones",
            "name"=> "Siberia v3",
            "price"=> 80,
            "active"=>true,
        ],
        [
            "type"=> "phone",
            "name"=> "Iphone 13",
            "price"=> 960,
            "active"=>true,
        ],
        [
            "type"=> "laptop",
            "name"=>"DELL hzsy",
            "price"=>220,
            "active"=>false,
        ]
    ];
?>
<script>
    let product = <?php echo json_encode($products) ?>;
    //debugger;
</script>