<?php

    $email = isset($_GET['email'])?$_GET['email']:"";
    $area = isset($_GET['area'])?$_GET['area']:"";

    $validEmpty = $email == "";
    $validFormat = filter_var($email, FILTER_VALIDATE_EMAIL);
    $validAreaEmpty = $area == ""; 
    
    echo json_encode([
        'validEmpty' => $validEmpty,
        'validFormat' => $validFormat,
        'validAreaEmpty' =>$validAreaEmpty 
    ]);
    
?>
